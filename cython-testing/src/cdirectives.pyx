# cython: boundscheck = True
# cython: cdivision = False

import cython

cdef double func1(double[::1] mv):
    cdef double s = 1.0
    cdef unsigned int i
    for i in range(mv.shape[0]):
        s /= mv[i]

    return s

@cython.boundscheck(True)
@cython.cdivision(False)
cdef double func2(double[::1] mv):
    cdef double s = 1.0
    cdef unsigned int i
    for i in range(mv.shape[0]):
        s /= mv[i]

    return s