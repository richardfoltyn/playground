__author__ = 'Richard Foltyn'

from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import Extension, build_ext


ext_module = Extension(
    "memviewtest",
    ["memviewtest.pyx"],
    extra_compile_args=['-fopenmp'],
    extra_link_args=['-fopenmp']
)

typetest = Extension("typetest",
                     ["typetest.pyx"])

polymorph = Extension("polymorph",
                      ["polymorph.pyx"])

condcomp = Extension("condcomp", ["condcomp.pyx"])

cdirective = Extension("cdirectives", ["cdirectives.pyx"],
                       cython_directives={'boundscheck': False})

setup(name='scratch',
      ext_modules=cythonize([ext_module, typetest, polymorph,
                             condcomp, cdirective],
                            include_path=['..']))