"""
Renamr: Renames and moves files to subdirectories based on their date.

Author: Richard Foltyn
"""
from __future__ import print_function, absolute_import, division

import argparse
import os.path
import os
from os.path import join
import shutil
from datetime import date, timedelta

import re

fn_regex = re.compile(r'^(?P<prefix>LT[\d]{7})(?P<year>[\d]{4})(?P<doy>[\d]{3}).*\.(?P<ext>[\w]{3,4})$')


def process_dir(path, dstdir):

    for name in os.listdir(path):
        p = join(path, name)
        if os.path.isdir(p):
            process_dir(p, dstdir)
        else:
            m = fn_regex.match(name)
            if m:
                process_file(p, m.groups(), dstdir)


def process_file(path, components, dstdir):
    prefix, year, doy, ext = components

    year = int(year)
    doy = int(doy)

    filename = '{:s}{:04d}{:03d}.{:s}'.format(prefix, year, doy, ext)

    # determine days in each month
    d = date(year, 1, 1) + timedelta(doy - 1)

    dirname = '{:04d}-{:02d}'.format(d.year, d.month)
    dstpath = join(dstdir, dirname)

    if not os.path.isdir(dstpath):
        os.makedirs(dstpath)

    # absolute path of destination file
    p = join(dstpath, filename)

    print('Moving "{}" to "{}"'.format(path, p))
    shutil.move(path, p)


def main():

    args = parse()
    srcdir = args.src
    dstdir = args.dst

    if not os.path.isdir(dstdir):
        os.makedirs(dstdir)

    process_dir(srcdir, dstdir)


def parse():

    p = argparse.ArgumentParser()
    p.add_argument('-s', '--src-dir', required=True,
                   dest='src', help='Source directory')
    p.add_argument('-d', '--dst-dir', required=True,
                   dest='dst', help='Destination directory')

    args = p.parse_args()
    return args

if __name__ == '__main__':
    main()