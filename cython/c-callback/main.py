"""
INSERT MODULE DOCSTRING HERE

Author: Richard Foltyn
"""
from __future__ import print_function, division, absolute_import
import numpy as np


from wrapper import Wrapper
from dispatcher import call_solve


class SubWrapper(Wrapper):
    """
    Subclass of Wrapper. Contains whatever parameters etc. is needed to
    compute stuff in objective()
    """

    def __init__(self, param1, param2):
        self.param1 = param1
        self.param2 = param2

    def objective(self, z, f):
        """
        Objective function. Returns whatever the C library needs in array f.
        """
        print('Called SubWrapper::objective')
        status = 123

        f = np.ascontiguousarray(f)
        z = np.ascontiguousarray(z)

        f[:] = self.param1 + self.param2 * z

        return status


def main():

    param1 = 1.234
    param2 = 4.234
    n = 100

    wrapper = SubWrapper(param1, param2)
    zz = np.linspace(0.0, 1.0, n)
    ff = np.empty_like(zz)

    call_solve(wrapper, zz, ff)

    print('Completed call')


if __name__ == '__main__':
    main()
