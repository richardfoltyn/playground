
#include <stdio.h>
#include "interface.h"

void solve(objective_t fcn, int n, double *z, double *f) {

	int status, i;

	status = fcn(n, z, f);

	printf("C: status code: %d\n", status);

	printf("C: Contents of array f:\n");
	for (i = 0; i < n; i++) {
		printf("%10.5f\n", f[i]);
	}

}