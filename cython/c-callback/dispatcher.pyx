

from wrapper cimport Wrapper

cdef extern from 'interface.h':
    ctypedef int (*objective_t) (int, double *, double*)
    int solve(objective_t, int, double *, double*)

cdef Wrapper w


def call_solve(wrapper, double[::1] zz, double[::1] ff):
    """
    Set up global pointer to Wrapper object and call C function solve()
    """
    cdef int status
    cdef int n = zz.shape[0]
    global w
    w = wrapper
    solve (f_dispatch, n, &(zz[0]), &(ff[0]))

cdef int f_dispatch(int n, double *z, double *f):
    """
    Called from C code, will dispatch function call to objective() method
    of glocal Wrapper instance.
    """

    cdef status
    cdef double[::1] zz, ff

    ff = <double[:n]>f
    zz = <double[:n]>z

    status = w.objective(zz, ff)
    return status