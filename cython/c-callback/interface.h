
#pragma once

typedef int (*objective_t) (int, double *z, double *f);

void solve(objective_t, int, double *, double *);
