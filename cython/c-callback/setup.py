from distutils.core import setup
import os.path
from os.path import join
import sys

from Cython.Build import cythonize
from Cython.Distutils import Extension

ext = [Extension('*', ['wrapper.pyx']),
       Extension('*', ['dispatcher.pyx', 'solver.c'])]

setup(name='test',
      ext_modules=cythonize(ext, annotate=True))
