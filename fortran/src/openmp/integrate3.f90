program integrate

    use iso_fortran_env
    use omp_lib
    implicit none

    integer, parameter :: nsteps = int(1e7)
    real (real64) :: pi_approx, step, x, t0, t1
    integer :: i

    step = 1.0d0 / nsteps

    t0 = omp_get_wtime ()

    !$omp parallel do private(x) schedule(auto) reduction(+: pi_approx)
    do i = 0, nsteps-1
        x = (i + 0.5d0) * step
        pi_approx = pi_approx + 4.0d0 / (1.0d0 + x ** 2)
    end do

    t1 = omp_get_wtime () - t0

    pi_approx = pi_approx * step
    print '("Pi = ", g0.10)', pi_approx
    print '("Elapsed time: ", g0.2, " seconds")', t1

end
