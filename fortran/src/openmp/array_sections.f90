module mod1

    use iso_fortran_env
    implicit none
    private

    public :: do_work

    integer, parameter :: PREC = real64

    abstract interface
        subroutine fcn_real64 (x)
            import real64
            real (real64), intent(out), dimension(:,:) :: x
        end subroutine
    end interface

contains

subroutine solver (fcn, m, n, tot)
    procedure (fcn_real64) :: fcn
    integer, intent(in) :: m, n
    real (PREC), dimension(:,:), allocatable :: x
    real (PREC), intent(out) :: tot

    allocate (x(m,n), source=0.0_PREC)

    call fcn (x)

    tot = sum(x) / size(x)

end subroutine

subroutine do_work (arr1, arr2, tot)
    real (PREC), intent(in), dimension(:,:) :: arr1, arr2
    real (PREC), intent(out) :: tot

    real (PREC) :: alpha, beta

    alpha = 2.0d0
    beta = 1.0d0

    call solver (objective, size(arr1, 1), size(arr2, 2), tot)

contains

    subroutine objective (x)
        real (PREC), intent(out), dimension(:,:) :: x

        integer :: i, j, k

        do j = 1, size(arr2, 2)
            do i = 1, size(arr1, 1)
                do k = 1, size(arr1, 2)
                    x(i,j) = alpha * arr1(i,k) * arr2(k,j) + beta * x(i,j)
                end do
            end do
        end do
    end subroutine
end subroutine

end module


program omp_array_demo

    use iso_fortran_env
    use mod1

    integer, parameter :: PREC = real64

    integer, parameter :: d1 = 100, d2 = 100, d3 = 10, d4 = 100
    real (PREC), dimension(:,:,:,:), allocatable :: arr1, arr2
    real (PREC), dimension(:,:), allocatable :: res
    integer :: i, j
    real (PREC) :: tot

    allocate (arr1(d1,d2,d3,d4), arr2(d1,d2,d3,d4))
    allocate (res(d3,d4))

    call random_number (arr1)
    call random_number (arr2)

    !$omp parallel default(none) &
    !$omp private(i,j,tot) &
    !$omp shared(arr1,arr2,res)

    !$omp do
    do j = 1, d4
        do i = 1, d3
            call do_work (arr1(:,:,i,j), arr2(:,:,i,j), tot)
            res(i, j) = tot
        end do
    end do
    !$omp end do
    !$omp end parallel

    print '(*(10(f0.3, :, " "), /))', res


end
