! Improved version of computing Pi via integration.
program integrate2

    use iso_fortran_env
    use omp_lib
    implicit none

    integer, parameter :: nsteps = int(1e7)
    real (real64) :: pi_approx, step, x, t0, t1, fx, pi_i
    integer :: i, nthreads, tid, ifrom, ito, insteps

    step = 1.0d0 / nsteps
    nthreads = omp_get_num_threads ()
    t0 = omp_get_wtime ()

    insteps = nsteps / nthreads
    pi_approx = 0.0

    !$omp parallel &
    !$omp private (tid, ifrom, ito, i, x, pi_i)

    tid = omp_get_thread_num ()

    ifrom = tid * insteps + 1
    if (tid < nthreads - 1) then
        ito = (tid + 1) * insteps
    else
        ito = nsteps
    end if

    pi_i = 0.0
    do i = ifrom-1, ito-1
        x = (i + 0.5d0) * step
        fx = 4.0d0 / (1.0d0 + x ** 2)
        pi_i = pi_i + fx
    end do

    !$omp atomic update
    pi_approx = pi_approx + pi_i
    !$omp end atomic

    !$omp end parallel
    pi_approx = pi_approx * step
    t1 = omp_get_wtime () - t0

    print '("Pi = ", g0.10)', pi_approx
    print '("Elapsed time: ", g0.2, " seconds")', t1

end
