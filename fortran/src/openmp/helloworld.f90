program openmp_test

    use omp_lib
    implicit none

    integer :: i, nthreads


    !$omp parallel default (none) &
    !$omp shared (nthreads) &
    !$omp private (i)

    !$omp master
    nthreads = omp_get_num_threads ()
    print *, "Total number of threads: ", nthreads
    !$omp end master

    i = omp_get_thread_num ()
    print *, "Thread ", i
    !$omp end parallel

end
