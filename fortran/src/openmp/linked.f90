program linked

    use iso_fortran_env
    use omp_lib
    implicit none

    integer, parameter :: N = 5
    integer, parameter :: FS = 38

    type :: llist
        type (llist), pointer :: next => null()
        integer :: data, fibdata
    end type

    class (llist), pointer :: ptr_head, ptr_n, ptr_tmp
    real (real64) :: t0, t1

    t0 = omp_get_wtime ()

    allocate (ptr_head)
    call init_list (ptr_head)

    ptr_n => ptr_head
    !$omp parallel
    !$omp single
    do while (associated(ptr_n))
        !$omp task firstprivate(ptr_n)
        call process (ptr_n)
        !$omp end task
        ptr_n => ptr_n%next
    end do
    !$omp end single
    !$omp end parallel

    ptr_n => ptr_head
    do while (associated(ptr_n))
        print '(i0, ": ", i0)', ptr_n%data, ptr_n%fibdata

        ptr_tmp => ptr_n%next
        deallocate (ptr_n)
        ptr_n => ptr_tmp
    end do

    t1 = omp_get_wtime () - t0
    print '("Elapsed time: ", f5.2, " seconds")', t1

contains

pure subroutine init_list (node)
    type (llist), intent(in out), target :: node

    class (llist), pointer :: ptr_n
    integer :: i

    ptr_n => node

    do i = 0, N
        ptr_n%data = FS + i
        ptr_n%fibdata = i

        if (i < N) then
            allocate (ptr_n%next)
            ptr_n => ptr_n%next
        else
            nullify (ptr_n%next)
        end if
    end do
end subroutine

recursive function fib (n) result(res)
    integer, intent(in) :: n
    integer :: res

    integer :: x, y
    res = n
    if (n < 2) return

    x = fib (n-1)
    y = fib (n-2)
    res = x + y
end function

subroutine process (dat)
    type (llist), intent(in out) :: dat

    dat%fibdata = fib (dat%data)
end subroutine

end
