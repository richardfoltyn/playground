program mandel

    use iso_fortran_env
    use omp_lib
    implicit none

    integer, parameter :: npoints = 1000
    integer, parameter :: maxiter = 1000

    integer :: noutside, i, j
    real (real64) :: area, error, im, re
    real (real64), parameter :: eps = 1.0d-5
    complex (real64) :: c
    real (real64) :: t0, t1

    noutside = 0
    !t0 = omp_get_wtime ()

    !$omp parallel do schedule(auto) private(i, j, c) reduction(+: noutside)
    do i = 0, npoints-1
        do j = 0, npoints-1
            re = -2.0d0 + 2.5d0 * real(i, real64) / npoints + eps
            im = 1.125 * real(j, real64) / npoints + eps
            c = cmplx(re, im, real64)
            noutside = noutside + testpoint (c)
        end do
    end do

    area = 2.0d0 * 2.5d0 * 1.125d0 * (npoints ** 2.0d0 - noutside) / (npoints ** 2.0d0)
    error = area / npoints

    !t1 = omp_get_wtime () - t0

    print '("Area of Mandelbrot set = ", f12.8, " +/- ", f12.8)', area, error
    print *, "Correct answer should be around 1.510659"
    !print '("Elapsed time: ", g0.2, " seconds")', t1

contains

pure function testpoint(c) result(res)
    complex (real64), intent(in), value :: c
    integer :: res

    complex (real64) :: z
    integer :: i

    res = 0

    z = c
    do i = 1, maxiter
        z = z*z + c
        if (abs(z) > 2.0d0) then
            res = 1
            return
        end if
    end do

end function

end
