program omp_nested

    use, intrinsic :: iso_fortran_env
    use omp_lib

    call main ()

    contains

subroutine main ()

    integer :: i, j

    !$omp parallel private (i, j)
    !$omp do collapse (2) ordered
    do i = 1, 10
        do j = 1, 10
            !$omp ordered
            print '(tr3, "i=", i2, "; j=", i2)', i, j
            !$omp end ordered
        end do
    end do
    !$omp end do
    !$omp end parallel

end subroutine

end program
