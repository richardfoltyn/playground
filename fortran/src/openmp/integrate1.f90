program integrate

    use iso_fortran_env
    use omp_lib
    implicit none

    integer, parameter :: nsteps = int(1e7)
    real (real64) :: Fx, step, x, t0, t1
    real (real64), dimension(:), allocatable :: Fxi
    integer :: i, ifrom, ito, insteps, nthreads, tid

    step = 1.0d0 / nsteps
    t0 = omp_get_wtime ()

    call omp_set_dynamic (.false.)
    !$omp parallel default(shared) private (tid, ifrom, ito, i, x)
    !$omp single
    nthreads = omp_get_num_threads ()
    print '("Using ", i0, " threads")', nthreads
    insteps = nsteps / nthreads
    allocate (Fxi(nthreads))

    !$omp end single
    !$omp barrier
    
    tid = omp_get_thread_num ()
    ifrom = tid * insteps + 1
    if (tid < nthreads - 1) then
        ito = (tid + 1) * insteps
    else
        ito = nsteps
    end if

    print '("Thread ", i0, " summing from ", i0, " to ", i0)', tid, ifrom, ito

    Fxi(tid + 1) = 0.0d0
    do i = ifrom, ito
        x = (i + 0.5d0) * step
        Fxi(tid+1) = Fxi(tid+1) + 4.0d0 / (1.0d0 + x ** 2)
    end do

    !$omp end parallel

    t1 = omp_get_wtime () - t0

    Fx = sum(Fxi) * step
    print '("F(x) = ", g0.10)', Fx
    print '("Elapsed time: ", g0.2, " seconds")', t1

end
