module rng

    use iso_fortran_env
    implicit none
    private
    public :: rng_seed, rng_uniform

    ! Dimension of the state
    integer, parameter :: ns = 4

    ! Default seed vector
    integer, parameter, dimension(ns) :: default_seed &
        = [521288629, 362436069, 16163801, 1131199299]

    integer, dimension(NS), save :: state = default_seed
    !$omp threadprivate(state)

contains

    ! Seeds the RNG using a single integer and a default seed vector.
    subroutine rng_seed(seed)
        integer, intent(in) :: seed
        state(1) = seed
        state(2:ns) = default_seed(2:ns)
    end subroutine rng_seed

    ! Draws a uniform real number on [0,1].
    function rng_uniform() result(u)
        real (real64) :: u
        integer :: imz

        imz = state(1) - state(3)

        if (imz < 0) imz = imz + 2147483579

        state(1) = state(2)
        state(2) = state(3)
        state(3) = imz
        state(4) = 69069 * state(4) + 1013904243
        imz = imz + state(4)
        u = 0.5d0 + 0.23283064d-9 * imz
    end function rng_uniform

end module rng

program pi_mc

    use iso_fortran_env
    use omp_lib
    use rng
    implicit none

    integer (int64), parameter :: ntrials = int(1e8, real64)
    real (real64) :: pi, x, y, radius, point, frac
    integer (int64) :: nincirc
    integer (int64) :: i
    integer :: tid
    real (real64) :: t0, t1

    radius = 1.0d0

    t0 = omp_get_wtime ()
    nincirc = 0

    !$omp parallel private(x,y,i,point,tid) reduction(+: nincirc)
    tid = omp_get_thread_num ()
    call rng_seed (tid * 10000)
    !nincirc = 0
    !$omp do schedule(auto)
    do i = 1, ntrials
        y = rng_uniform ()
        x = rng_uniform ()
        point = x**2 + y**2

        if (point <= radius ** 2) nincirc = nincirc + 1
    end do
    !$omp end do

    !$omp end parallel
    frac = real(nincirc, real64) / ntrials
    pi = 4.0d0 * frac

    t1 = omp_get_wtime () - t0

    print '(i0, " trials, pi is ", f15.10)', ntrials, pi
    print '("Elapsed time: ", f6.2, " seconds")', t1
end
