cmake_minimum_required(VERSION 3.0)

# include C in project languages, needed for LAPACK detection
project(fortran-playground Fortran)
option(BUILD_MPI "Build MPI examples" OFF)
option(BUILD_OpenMP "Build OpenMP examples" OFF)

unset(_FFLAGS)
unset(_FFLAGS_DEBUG)

if (CMAKE_Fortran_COMPILER_ID STREQUAL "GNU" OR MINGW)
    set(GNU_WARN "-Wall -Wextra -Wimplicit-interface -Wimplicit-procedure \
        -Warray-temporaries -Wrealloc-lhs -Wno-compare-reals")
    set(_FFLAGS "${GNU_WARN} -march=native -mfpmath=sse")
    set(_FFLAGS_DEBUG "-fcheck=all -O0 -ggdb")
elseif (CMAKE_Fortran_COMPILER_ID STREQUAL "Intel")
    if (WIN32)
        set(_FFLAGS "/warn:all /standard-semantics /stand:f08 /QxHost")
		set(_FFLAGS_DEBUG "/check:all")
    else ()
        set (_FFLAGS "-warn all -standard-semantics -std08 -xHost")
        set(_FFLAGS_DEBUG "-check all")
    endif (WIN32)
endif ()

set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${_FFLAGS}")
set(CMAKE_Fortran_FLAGS_DEBUG "${CMAKE_Fortran_FLAGS_DEBUG} ${_FFLAGS_DEBUG}")
message("Compiler FLAGS: ${CMAKE_Fortran_FLAGS}")
message("Add. Release FLAGS: ${CMAKE_Fortran_FLAGS_RELEASE}")
message("Add. Debug FLAGS: ${CMAKE_Fortran_FLAGS_DEBUG}")


add_subdirectory(serial)
add_subdirectory(bugs)

find_package(MPI)
find_package(OpenMP)

if (OPENMP_FOUND AND BUILD_OpenMP)
    message(STATUS "Building OpenMP examples")
    add_subdirectory(openmp)
endif()

if (MPI_Fortran_FOUND AND BUILD_MPI)
    message(STATUS "Building MPI examples")
    add_subdirectory(mpi)
endif()