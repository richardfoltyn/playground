program test
    !*  Tests for gfortran bug that wrongly computes bounds on polymorphic
    !   array dummy arguments if the actual argument is a literal / temporary
    !   array.
    !   See https://gcc.gnu.org/bugzilla/show_bug.cgi?id=60322

    implicit none

    type :: type_t
        integer :: value = 0
    end type

    call test0 ()
    ! call test1 ()
    ! call test2 ()
    call test3 ()
    call test4 ()
    ! call test5 ()

contains


subroutine test0 ()
    !*  Test 0: Demonstrates that the bug is not triggered if the actual
    !   argument associated with the polymorphic array dummy is NOT an array
    !   "literal".

    type (type_t), dimension(3) :: x
    integer :: i

    print *, "Test 0"

    do i = 1, size(x)
        x(i)%value = i
    end do

    call process_polymorphic (x)

end subroutine


subroutine test1 ()

    print *, "Test 1"

    call process_polymorphic ([1, 2, 3])
    call process_polymorphic (["a", "b", "c"])
    ! call process_polymorphic ([type_t(1), type_t(2), type_t(3)])

end subroutine


subroutine test2 ()

    print *, "Test 2"

    call indirect_call ([type_t(1), type_t(2), type_t(3)])

end subroutine


subroutine test3 ()
    !*  Test whether "indirect" invocation works if another routine
    !   with polymorphic array dummy argument is inserted in the call sequence.
    !   Note: this should be unaffected by the bug if the original actual
    !   argument is not an array "literal"

    type (type_t), dimension(3) :: x
    integer :: i

    print *, "Test 3"

    do i = 1, size(x)
        x(i)%value = i
    end do

    call indirect_call (x)

end subroutine


subroutine test4 ()

    character (:), dimension(:), allocatable :: x

    allocate (character (1) :: x(3))

    print *, "Test 4: Allocatable character array"

    x(1) = "a"
    x(2) = "b"
    x(3) = "c"

    ! call process_polymorphic (x)
    call indirect_call (x)

end subroutine


subroutine test5 ()

    character (1), dimension(3) :: x

    print *, "Test 5: Local character array"

    x(1) = "a"
    x(2) = "b"
    x(3) = "c"

    call process_polymorphic (x)

end subroutine

subroutine indirect_call (val)
    class (*), intent(in), dimension(:) :: val

    call process_polymorphic (val)
end subroutine


subroutine process_polymorphic (obj)
    class (*), intent(in), dimension(:), target :: obj

    character (1), dimension(:), pointer :: cptr1
    integer, dimension(:), pointer :: iptr
    class (type_t), dimension(:), pointer :: ptr_t

    integer :: n, k
    integer :: lb(1), ub(1)

    nullify (cptr1, iptr, ptr_t)

    n = size(obj)
    lb = lbound(obj)
    ub = ubound(obj)

    print '("lb=", i0, "; ub=", i0, "; size=", i0)', lb(1), ub(1), n

    select type (obj)
    type is (character (*))
        cptr1 => obj
        k = len(cptr1)
        print '(a, i0)', "Character length: ", k
        print '(*(a, :, ", "))', cptr1
    type is (integer)
        iptr => obj
        print '(*(i0, :, ", "))', iptr
    type is (type_t)
        ptr_t => obj
        print '(*(i0, :, ","))', ptr_t%value
    end select

end subroutine

end
