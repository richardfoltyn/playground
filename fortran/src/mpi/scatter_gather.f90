
program mpi_reduce_demo

    use, intrinsic :: iso_fortran_env
    use mpi_f08

    implicit none

    integer, parameter :: PREC = real64

    integer, parameter :: ROOT_ID = 0

    integer, parameter :: TASK_COMPUTE = 1
    integer, parameter :: TASK_TERMINATE = 2

    integer, parameter :: MAX_DEGREE = 3

    call main ()

    contains

subroutine main ()

    integer :: ierr, nproc, proc_id
    real (PREC), parameter :: coefs(MAX_DEGREE+1) = &
        [ real(PREC) ::  1.23, -4.23, 6.78, -1.23]
    real (PREC), parameter :: LBOUNDS(*) = [ real(PREC) :: -10, 1, 10]
    real (PREC), parameter :: UBOUNDS(*) = [ real(PREC) :: 1, 10, 20]
    real (PREC) :: lb, ub, exact, interval, integral
    integer :: i, j, k, pid, task
    real (PREC), dimension(:), allocatable, target :: bounds_all
    real (PREC), dimension(:), allocatable :: results
    real (PREC), dimension(:,:),  pointer, contiguous :: ptr_bounds

    call MPI_INIT (ierr)
    call MPI_COMM_SIZE (MPI_COMM_WORLD, nproc, ierr)
    call MPI_COMM_RANK (MPI_COMM_WORLD, proc_id, ierr)

    task = TASK_COMPUTE

    allocate (bounds_all(2 * nproc), source=0.0_PREC)
    ptr_bounds(1:2,0:nproc-1) => bounds_all

    allocate (results(0:nproc-1), source=0.0_PREC)

    if (proc_id == ROOT_ID) then
        do i = 1, size(coefs)
            do j = 1, size(LBOUNDS)
                ! Communicate which task to perform
                call MPI_Bcast (task, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, ierr)

                ! Broadcast current number of coefs
                call MPI_Bcast (i, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, ierr)

                call MPI_Bcast (coefs(1:i), i, MPI_DOUBLE_PRECISION, proc_id, &
                    MPI_COMM_WORLD, ierr)

                interval  = UBOUNDS(j) - LBOUNDS(j)

                print 100, LBOUNDS(j), UBOUNDS(j), i

                ! Prepare bounds array for scatter
                do pid = 1, nproc - 1
                    lb = LBOUNDS(j) + interval / (nproc-1) * (pid-1)
                    ub = lb + interval / (nproc-1)
                    ptr_bounds(1,pid) = lb
                    ptr_bounds(2,pid) = ub
                end do

                call MPI_scatter (bounds_all, 2, MPI_DOUBLE_PRECISION, &
                    MPI_IN_PLACE, 2, MPI_DOUBLE_PRECISION, &
                    ROOT_ID, MPI_COMM_WORLD, ierr)

                ! Collect results from other instances
                call MPI_Gather (MPI_IN_PLACE, 1, MPI_DOUBLE_PRECISION, &
                    results, 1, MPI_DOUBLE_PRECISION, ROOT_ID, &
                    MPI_COMM_WORLD, ierr)

                integral = sum(results(1:))

                exact = 0.0
                lb = LBOUNDS(j)
                ub = UBOUNDS(j)
                do k = 1, i
                    exact = exact + coefs(k) * (ub**k - lb**k) / k
                end do
                print '(tr2, "Exact solution: ", f0.8)', exact
                print '(tr2, "Approximation:  ", f0.8)', integral
            end do
        end do

        task = TASK_TERMINATE
        call MPI_Bcast (task, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, ierr)

    else
        call slave_event_loop ()
    end if

    call MPI_FINALIZE (ierr)

100 format ("Computing integral on [", f0.1, ", ", f0.1, "] for ", i1, " coefs")

end subroutine


subroutine slave_event_loop ()
    real (PREC), dimension(MAX_DEGREE+1) :: buf
    integer :: ncoefs
    real (PREC) :: lb, ub, integral
    integer :: ierr, task
    real (PREC), dimension(2) :: bounds

    integer, parameter :: n = 10000

    do while (.true.)
        ! Retrieve task to perform
        call MPI_Bcast (task, 1, MPI_INTEGER, ROOT_ID, MPI_COMM_WORLD, ierr)

        if (task == TASK_TERMINATE) exit

        ! Retrieve number of coefficients
        call MPI_Bcast (ncoefs, 1, MPI_INTEGER, ROOT_ID, MPI_COMM_WORLD, ierr)

        ! Retrieve current coefficient array
        call MPI_BCAST (buf(1:ncoefs), ncoefs, MPI_DOUBLE_PRECISION, ROOT_ID, &
            MPI_COMM_WORLD, ierr)

        call MPI_scatter (bounds, 2, MPI_DOUBLE_PRECISION, &
            bounds, 2, MPI_DOUBLE_PRECISION, &
            ROOT_ID, MPI_COMM_WORLD, ierr)

        lb = bounds(1)
        ub = bounds(2)

        integral = slave_compute (buf(1:ncoefs), lb, ub, n)

        ! Send back results
        call MPI_Gather (integral, 1, MPI_DOUBLE_PRECISION, &
            integral, 1, MPI_DOUBLE_PRECISION, ROOT_ID, &
            MPI_COMM_WORLD, ierr)
    end do
end subroutine


function slave_compute (coefs, lb, ub, n) result(res)
    real (PREC), intent(in), dimension(:) :: coefs
    real (PREC), intent(in) :: lb, ub
    integer, intent(in) :: n

    real (PREC) :: res, width, x
    integer :: i

    res = 0.0

    width = (ub-lb) / n

    !$omp parallel default(none) &
    !$omp private (i,x) shared(n,lb,ub,coefs,width) &
    !$omp reduction(+: res)

    !$omp do
    do i = 1, n
        x = lb + (i-0.5_PREC) * width
        res = res + poly (coefs, x)
    end do
    !$omp end do
    !$omp end parallel

    res = res * width
end function


pure function poly (coefs, x) result(res)
    real (PREC), intent(in), dimension(0:) :: coefs
    real (PREC), intent(in) :: x
    real (PREC) :: res

    integer :: i
    res = 0.0

    do i = 0, size(coefs) - 1
        res = res + coefs(i) * x ** i
    end do

end function

end