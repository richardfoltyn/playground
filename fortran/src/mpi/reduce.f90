
program mpi_reduce_demo

    use, intrinsic :: iso_fortran_env
    use mpi_f08

    implicit none

    integer, parameter :: PREC = real64

    integer, parameter :: ROOT_ID = 0

    integer, parameter :: TASK_COMPUTE = 1
    integer, parameter :: TASK_TERMINATE = 2

    integer, parameter :: MAX_DEGREE = 3

    call main ()

    contains

subroutine main ()

    integer :: err, nproc, proc_id
    real (PREC), parameter :: coefs(MAX_DEGREE+1) = &
        [ real(PREC) ::  1.23, -4.23, 6.78, -1.23]
    real (PREC), parameter :: LBOUNDS(*) = [ real(PREC) :: -10, 1, 10]
    real (PREC), parameter :: UBOUNDS(*) = [ real(PREC) :: 1, 10, 20]
    real (PREC) :: lb, ub, exact, interval
    integer :: i, j, k, pid, task
    integer, parameter :: tag = 1

    real (PREC) :: ltotal, total

    call MPI_INIT (err)
    call MPI_COMM_SIZE (MPI_COMM_WORLD, nproc, err)
    call MPI_COMM_RANK (MPI_COMM_WORLD, proc_id, err)

    task = TASK_COMPUTE

    if (proc_id == ROOT_ID) then
        do i = 1, size(coefs)
            do j = 1, size(LBOUNDS)
                ! Communicate which task to perform
                call MPI_Bcast (task, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, err)

                ! Broadcast current number of coefs
                call MPI_Bcast (i, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, err)

                call MPI_Bcast (coefs(1:i), i, MPI_DOUBLE_PRECISION, proc_id, &
                    MPI_COMM_WORLD, err)

                interval  = UBOUNDS(j) - LBOUNDS(j)

                print '("Computing integral on [", f0.1, ", ", f0.1, "] for ", i1, " coefs")', &
                    LBOUNDS(j), UBOUNDS(j), i

                do pid = 1, nproc - 1
                    ! Communicate task-specific lower and upper bounds
                    lb = LBOUNDS(j) + interval / (nproc-1) * (pid-1)
                    ub = lb + interval / (nproc-1)
                    call MPI_Send (lb, 1, MPI_DOUBLE_PRECISION, pid, tag, &
                        MPI_COMM_WORLD, err)
                    call MPI_Send (ub, 1, MPI_DOUBLE_PRECISION, pid, tag, &
                        MPI_COMM_WORLD, err)
                end do

                call MPI_Reduce (ltotal, total, 1, MPI_DOUBLE_PRECISION, &
                    MPI_SUM, ROOT_ID, MPI_COMM_WORLD, err)

                exact = 0.0
                lb = LBOUNDS(j)
                ub = UBOUNDS(j)
                do k = 1, i
                    exact = exact + coefs(k) * (ub**k - lb**k) / k
                end do
                print '(tr2, "Exact solution: ", f0.8)', exact
                print '(tr2, "Approximation:  ", f0.8)', total
            end do
        end do

        task = TASK_TERMINATE
        call MPI_Bcast (task, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, err)

    else
        call slave_event_loop (proc_id)
    end if

    call MPI_FINALIZE (err)
end subroutine


subroutine slave_event_loop (proc_id)
    integer, intent(in) :: proc_id
    real (PREC), dimension(MAX_DEGREE+1) :: buf
    integer :: ncoefs
    real (PREC) :: lb, ub, integral, total
    integer :: err, task
    type (MPI_Status) :: status
    integer, parameter :: tag = 1

    integer, parameter :: n = 10000

    do while (.true.)
        ! Retrieve task to perform
        call MPI_Bcast (task, 1, MPI_INTEGER, ROOT_ID, MPI_COMM_WORLD, err)

        if (task == TASK_TERMINATE) exit

        ! Retrieve number of coefficients
        call MPI_Bcast (ncoefs, 1, MPI_INTEGER, ROOT_ID, MPI_COMM_WORLD, err)

        ! Retrieve current coefficient array            do j = 1, size(LBOUNDS)
        call MPI_BCAST (buf(1:ncoefs), ncoefs, MPI_DOUBLE_PRECISION, ROOT_ID, &
            MPI_COMM_WORLD, err)

        call MPI_Recv (lb, 1, MPI_DOUBLE_PRECISION, ROOT_ID, tag, &
            MPI_COMM_WORLD, status, err)

        call MPI_Recv (ub, 1, MPI_DOUBLE_PRECISION, ROOT_ID, tag, &
            MPI_COMM_WORLD, status, err)

        integral = slave_compute (buf(1:ncoefs), lb, ub, n)

        call MPI_Reduce (integral, total, 1, MPI_DOUBLE_PRECISION, &
            MPI_SUM, ROOT_ID, MPI_COMM_WORLD, err)
    end do
end subroutine


function slave_compute (coefs, lb, ub, n) result(res)
    real (PREC), intent(in), dimension(:) :: coefs
    real (PREC), intent(in) :: lb, ub
    integer, intent(in) :: n

    real (PREC) :: res, width, x
    integer :: i

    res = 0.0

    width = (ub-lb) / n

    !$omp parallel default(none) &
    !$omp private (i,x) shared(n,lb,ub,coefs,width) &
    !$omp reduction(+: res)

    !$omp do
    do i = 1, n
        x = lb + (i-0.5_PREC) * width
        res = res + poly (coefs, x)
    end do
    !$omp end do
    !$omp end parallel

    res = res * width
end function


pure function poly (coefs, x) result(res)
    real (PREC), intent(in), dimension(0:) :: coefs
    real (PREC), intent(in) :: x
    real (PREC) :: res

    integer :: i
    res = 0.0

    do i = 0, size(coefs) - 1
        res = res + coefs(i) * x ** i
    end do

end function

end