
program hellO_world

    use iso_fortran_env
    use mpi
    implicit none

    call main ()

    contains

subroutine main ()

    integer :: err
    integer :: proc_id, slave_id
    integer :: nproc
    integer :: i
    integer, dimension(mpi_status_size) :: status

    call mpi_init (err)
    call mpi_comm_size (mpi_comm_world, nproc, err)
    call mpi_comm_rank (mpi_comm_world, proc_id, err)

    if (proc_id == 0) then
        print *, 'Hello from process ', proc_id, ' of ', nproc , ' processes'

        do i = 1, nproc - 1
            call mpi_recv (slave_id, 1, mpi_integer, i, 1, mpi_comm_world, &
                    status, err)

            if (err /= MPI_SUCCESS) then
                print *, 'Error when calling MPI_RECV'
            end if

            print *, ' Hello from process ', slave_id, ' of ', nproc, ' processes'
        end do
    else
        call mpi_send (proc_id, 1, mpi_integer, 0, 1, mpi_comm_world, err)
        if (err /= MPI_SUCCESS) then
            print *, 'Error when calling MPI_SEND'
        end if
    end if

    call mpi_finalize (err)
end subroutine

end