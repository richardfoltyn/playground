
program mpi_reduce_types_demo
    !*  Demo of how to use MPI_Reduce in combination with transferring
    !   derived data types.

    use, intrinsic :: iso_fortran_env
    use mpi_f08

    implicit none

    integer, parameter :: PREC = real64

    integer, parameter :: ROOT_ID = 0

    integer, parameter :: TASK_COMPUTE = 1
    integer, parameter :: TASK_TERMINATE = 2

    integer, parameter :: MAX_DEGREE = 3

    type :: task_data
        !*  TASK_DATA stores information that specifies task to be performed
        !   by "slave" instances.
        sequence
        real (PREC) :: lb, ub
        real (PREC), dimension(MAX_DEGREE+1) :: coefs
        integer :: n
    end type

    call main ()

    contains


subroutine create_task_data_type (type_id)
    !*  CREATE_TASK_DATA_TYPE creates and MPI derived type that corresponds
    !   to Fortran's TASK_DATA.
    type (MPI_Datatype), intent(inout) :: type_id

    integer, parameter :: TYPE_LEN = 4
    type (MPI_Datatype), dimension(TYPE_LEN) :: data_types
    integer (MPI_ADDRESS_KIND), dimension(TYPE_LEN) :: displacements
    integer, dimension(TYPE_LEN) :: block_len

    integer :: err

    data_types(1:3) = MPI_DOUBLE_PRECISION
    data_types(4) = MPI_INTEGER

    block_len = 1
    block_len(3) = MAX_DEGREE+1

    displacements(1) = 0
    displacements(2) = displacements(1) + storage_size(0.0_PREC) / 8
    displacements(3) = displacements(2) + storage_size(0.0_PREC) / 8
    displacements(4) = displacements(3) + storage_size(0.0_PREC) / 8 * (MAX_DEGREE+1)

    call MPI_Type_create_struct (TYPE_LEN, block_len, displacements, &
        data_types, type_id, err)

    call MPI_Type_commit (type_id, err)

end subroutine



subroutine main ()
    !*  Uses MPI and OpenMP to numerically approximate integrals of polynomials.
    !   Integration is performed for polynomials with increasing degree
    !   over different integration intervals to demonstrate how task parameters
    !   can be communicated to participating MPI instances.

    integer :: err, nproc, proc_id
    real (PREC), parameter :: coefs(MAX_DEGREE+1) = &
        [ real(PREC) ::  1.23, -4.23, 6.78, -1.23]
    real (PREC), parameter :: LBOUNDS(*) = [ real(PREC) :: -10, 1, 10]
    real (PREC), parameter :: UBOUNDS(*) = [ real(PREC) :: 1, 10, 20]
    real (PREC) :: lb, ub, exact, interval
    integer :: i, j, k, pid, task
    integer, parameter :: tag = 1
    type (MPI_Datatype) :: task_data_type
    type (task_data) :: dat

    real (PREC) :: ltotal, total

    call MPI_INIT (err)
    call MPI_COMM_SIZE (MPI_COMM_WORLD, nproc, err)
    call MPI_COMM_RANK (MPI_COMM_WORLD, proc_id, err)

    ! Create derived MPI type corresponding to TASK_DATA
    call create_task_data_type (task_data_type)

    task = TASK_COMPUTE

    dat%coefs = coefs

    if (proc_id == ROOT_ID) then
        do i = 1, size(coefs)

            ! Set number of coefficients (polynomial degree) to use in current
            ! iteration
            dat%n = i

            do j = 1, size(LBOUNDS)
                ! Communicate which task to perform
                call MPI_Bcast (task, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, err)

                interval  = UBOUNDS(j) - LBOUNDS(j)
                print 100, LBOUNDS(j), UBOUNDS(j), i

                do pid = 1, nproc - 1
                    ! Communicate task-specific lower and upper bounds
                    lb = LBOUNDS(j) + interval / (nproc-1) * (pid-1)
                    ub = lb + interval / (nproc-1)

                    dat%lb = lb
                    dat%ub = ub
                    call MPI_Send (dat, 1, task_data_type, pid, tag, &
                        MPI_COMM_WORLD, err)
                end do

                call MPI_Reduce (ltotal, total, 1, MPI_DOUBLE_PRECISION, &
                    MPI_SUM, ROOT_ID, MPI_COMM_WORLD, err)

                exact = 0.0
                lb = LBOUNDS(j)
                ub = UBOUNDS(j)
                do k = 1, i
                    exact = exact + coefs(k) * (ub**k - lb**k) / k
                end do
                print '(tr2, "Exact solution: ", f0.8)', exact
                print '(tr2, "Approximation:  ", f0.8)', total
            end do
        end do

        task = TASK_TERMINATE
        call MPI_Bcast (task, 1, MPI_INTEGER, proc_id, MPI_COMM_WORLD, err)

    else
        call slave_event_loop (proc_id, task_data_type)
    end if

    ! Release MPI datatype corresponding to TASK_DATA
    call MPI_Type_free (task_data_type, err)

    call MPI_FINALIZE (err)

100 format ("Computing integral on [", f0.1, ", ", f0.1, "] for ", i1, " coefs")

end subroutine


subroutine slave_event_loop (proc_id, task_data_type)
    integer, intent(in) :: proc_id
    type (MPI_Datatype), intent(in) :: task_data_type

    integer :: ncoefs
    type (task_data) :: dat
    real (PREC) :: lb, ub, integral, total
    integer :: err, task
    type (MPI_Status) :: status
    integer, parameter :: tag = 1

    integer, parameter :: n = 1000000

    do while (.true.)
        ! Retrieve task to perform
        call MPI_Bcast (task, 1, MPI_INTEGER, ROOT_ID, MPI_COMM_WORLD, err)

        if (task == TASK_TERMINATE) exit

        call MPI_Recv (dat, 1, task_data_type, ROOT_ID, tag, &
            MPI_COMM_WORLD, status, err)

        ncoefs = dat%n
        lb = dat%lb
        ub = dat%ub

        integral = slave_compute (dat%coefs(1:ncoefs), lb, ub, n)

        call MPI_Reduce (integral, total, 1, MPI_DOUBLE_PRECISION, &
            MPI_SUM, ROOT_ID, MPI_COMM_WORLD, err)
    end do
end subroutine


function slave_compute (coefs, lb, ub, n) result(res)
    real (PREC), intent(in), dimension(:) :: coefs
    real (PREC), intent(in) :: lb, ub
    integer, intent(in) :: n

    real (PREC) :: res, width, x
    integer :: i

    res = 0.0

    width = (ub-lb) / n

    !$omp parallel default(none) &
    !$omp private (i,x) shared(n,lb,ub,coefs,width) &
    !$omp reduction(+: res)

    !$omp do
    do i = 1, n
        x = lb + (i-0.5_PREC) * width
        res = res + poly (coefs, x)
    end do
    !$omp end do
    !$omp end parallel

    res = res * width
end function


pure function poly (coefs, x) result(res)
    real (PREC), intent(in), dimension(0:) :: coefs
    real (PREC), intent(in) :: x
    real (PREC) :: res

    integer :: i
    res = 0.0

    do i = 0, size(coefs) - 1
        res = res + coefs(i) * x ** i
    end do

end function

end