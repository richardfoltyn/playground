program main

    use iso_fortran_env
    implicit none

    character (10) :: buf
    integer, target :: int
    real (real64), target :: r64
    real (real32), target :: r32
    logical, target :: bool

    class (*), pointer :: ptr

    buf = "123"

    ! integer test
    ptr => int
    read (unit=buf, fmt=*) ptr
    print *, int

end program
