program c_binding

    use iso_c_binding
    implicit none

    type (c_ptr) :: ptr

    integer, dimension(10,10) :: val
    integer, dimension(:), pointer :: ptr_val

    integer :: i, j, k
    !
    ! k = 1
    ! do j = 1, size(val, 2)
    !     do i = 1, size(val, 1)
    !         val(i, j) = k
    !         k = k + 1
    !     end do
    ! end do

    val(:,:) = reshape([(i, i = 1, size(val))], shape=shape(val))

    ptr = c_loc (val)

end program
