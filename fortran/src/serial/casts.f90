module mod1
    implicit none

    type :: base
        character (10) :: name
    contains
        procedure, pass :: print => base_print
    end type

    type, extends(base) :: child1
    contains
        procedure, pass :: print => child1_print
    end type

    type, extends(child1) :: child2
    contains
        procedure, pass :: print => child2_print
    end type

    interface dynamic_cast
        module procedure cast_any_to_base, cast_any_to_child1, cast_any_to_child1_array
    end interface

contains

    subroutine base_print (self)
        class (base), intent(in) :: self
        print *, "Base: ", self%name
    end subroutine

    subroutine child1_print (self)
        class (child1), intent(in) :: self

        print *, "Child 1: ", self%name
    end subroutine

    subroutine child2_print (self)
        class (child2), intent(in) :: self
        print *, "Child 2: ", self%name
    end subroutine

    subroutine cast_any_to_child1 (from, to)
        class (*), intent(in), pointer :: from
        type (child1), intent(out), pointer :: to

        select type (from)
        class is (child1)
            to => from
        class default
            error stop "Unsupported cast"
        end select
    end subroutine

    subroutine cast_any_to_child1_array (tgt, ptr)
        class (*), intent(in), dimension(:), pointer :: tgt
        type (base), intent(out), dimension(:), pointer :: ptr

        select type (tgt)
        class is (base)
            ptr => tgt
        class default
            error stop "Unsupported cast"
        end select
    end subroutine

    subroutine cast_any_to_base (from, to)
        class (*), intent(in), pointer :: from
        type (base), intent(out), pointer :: to

        select type (from)
        class is (base)
            to => from
        class default
            error stop "Unsupported cast"
        end select
    end subroutine
end module


program casts

    use mod1
    implicit none

    type (base), target :: b
    type (child1), target :: c1
    type (child2), target :: c2

    class (child2), pointer :: ptr_c2
    class (child1), pointer :: ptr_c1
    class (base), pointer :: ptr_b
    class (*), pointer :: ptr

    type (base), pointer :: ptr_b_type
    type (child1), pointer :: ptr_c1_type

    type (base), dimension(2), target :: barr
    type (child1), dimension(2), target :: c1arr

    type (child1), dimension(:), pointer :: ptr_c1arr
    type (base), dimension(:), pointer :: ptr_barr

    integer :: i

    b%name = "base"
    c1%name = "child 1"
    c2%name = "child 2"

    barr(:)%name = "base"
    c1arr(:)%name = "child 1"

    ! polymorphic invocation of print()
    ptr_b => c1
    call ptr_b%print ()

    ! non-polymorphic invocation, calls base::print() -- DOES NOT WORK
    ! ptr_b_type => c1
    ! call ptr_b_type%print ()

    ptr_c1 => c1
    call ptr_c1%base%print()

    ! upcasting
    ptr => c1
    call dynamic_cast (ptr, ptr_b_type)
    call ptr_b_type%print ()

end program
