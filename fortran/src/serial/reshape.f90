! Tests of reshape() and its order argument
program reshape_test

    implicit none

    integer, dimension(2,3,4) :: x
    integer, dimension(4,3,2) :: x2

    integer :: i, j

    x = reshape([(i, i=1,size(x))], shape=shape(x))
    x2 = reshape(x, shape=shape(x2), order=[3,2,1])

    do i = 1, size(x, 3)
        print '("x(:,:,", i0, ")")', i
        do j = 1, size(x, 1)
            print *, x(j,:,i)
        end do
    end do

    do i = 1, size(x2, 3)
        print '("x(:,:,", i0, ")")', i
        do j = 1, size(x2, 1)
            print *, x2(j,:,i)
        end do
    end do

    print *, x(1,2,:)
    print *, x2(:,1,2)

end
