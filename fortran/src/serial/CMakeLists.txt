
if (CMAKE_Fortran_COMPILER_ID STREQUAL "GNU" OR MINGW)
    set(_FFLAGS "-std=f2008 -pedantic")
endif ()

set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${_FLAGS}")

set(SOURCE_FILES
    elemental.f90
    type_bound_procs.f90
    finalize_recursive.f90
    print_matrix.f90
)

foreach(_file IN LISTS SOURCE_FILES)
    get_filename_component(exe_name "${_file}" NAME_WE)
    add_executable(${exe_name} "${_file}")
endforeach()

