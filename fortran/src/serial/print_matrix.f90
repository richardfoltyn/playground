

program print_matrix_demo

    implicit none


    integer, parameter :: NDIM = 4
    integer, dimension(NDIM,NDIM) :: mat
    integer :: i, j


    do j = 1, NDIM
        do i = 1, NDIM
            mat(i,j) = j + (i-1) * NDIM
        end do
    end do

    print '(*(tr8, 4(i2, :, ", "), /))', transpose(mat)

end