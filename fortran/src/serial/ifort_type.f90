module mod_parent
    implicit none

    type, abstract :: parent_t (k)
        integer, kind :: k = kind(1.0d0)
    contains
        procedure (sub1_iface), public, deferred, pass :: sub1
    end type

    interface
        elemental function sub1_iface (self, x) result(res)
            import parent_t
            integer, parameter :: PREC = kind(1.0d0)
            class (parent_t (k=PREC)), intent(in) :: self
            real (PREC), intent(in) :: x
            real (PREC) :: res
        end function
    end interface

end module

module mod1

    use iso_fortran_env
    use mod_parent
    implicit none
    private

    type, public, extends(parent_t) :: child_t
        real (k) :: val = 1.0_k
    contains
        procedure, public, pass :: sub1
    end type

    type (child_t), public, protected :: obj = child_t(2.0d0)

contains

elemental function sub1 (self, x) result(res)
    integer, parameter :: PREC = real64
    class (child_t(PREC)), intent(in) :: self
    real (PREC), intent(in) :: x
    real (PREC) :: res
    res = self%val * x
end function

end module


program test

    use mod1

    type (child_t) :: foo

    print *, foo%sub1 (10.0d0)

end program
