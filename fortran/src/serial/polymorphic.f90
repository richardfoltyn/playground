program polymorphic

    implicit none

    integer, dimension(5), target :: src, dst
    class (*), dimension(:), pointer :: ptr

    integer :: int_src

    interface process_poly
        procedure process_poly_scalar, process_poly_array
    end interface

    int_src = 10
    call process_poly (int_src)

contains

subroutine to_int32 (src, dst)
    class (*), intent(in), dimension(:) :: src
    integer, intent(out), dimension(:) :: dst

    select type (src)
    type is (integer)
        dst = src
    class default
        error stop "Unsupported cast"
    end select
end subroutine

subroutine process_poly_scalar (x)
    class (*), intent(in) :: x

    class (*), dimension(:), allocatable :: xx

    allocate (xx(2), source=x)

    call process_poly (xx)

end subroutine

subroutine process_poly_array (x)
    class (*), intent(in), dimension(:), target :: x
    integer, dimension(:), pointer :: ptr

    select type (x)
    type is (integer)
        ptr => x
        print *, ptr
    end select
end subroutine

end program
