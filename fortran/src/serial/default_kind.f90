program default_kind

    use iso_fortran_env
    implicit none

    integer, parameter :: i1 = selected_int_kind(2)
    integer, parameter :: i2 = selected_int_kind(5)
    integer, parameter :: i4 = selected_int_kind(9)
    integer, parameter :: i8 = selected_int_kind(12)
    ! integer, parameter :: i16 = selected_int_kind(20)

    integer, parameter :: r2 = selected_real_kind(7)

    real (r2) :: y2

    integer (i1) :: x1
    integer (i2) :: x2
    integer (i4) :: x4
    integer (i8) :: x8

    print "(i0)", i1, i2, i4, i8

    print *, int8, int16, int32, int64

    print *, bit_size(x1)
    print *, bit_size(x2)
    print *, bit_size(x4)
    print *, bit_size(x8)

    print *, precision(y2)

end program
