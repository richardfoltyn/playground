! Some test code for polymorphic usage of type-bound procedures.

module test1_mod

    type :: foo
    contains
        procedure, pass :: sub => foo_sub
    end type
contains

subroutine foo_sub (self)
    class (foo), intent(in) :: self
    ! Note: type(foo) does not compile!
end subroutine

end module

program test1

    use test1_mod

end program
