program routines

    use iso_fortran_env
    implicit none

    integer, parameter :: N = 1000

    real (real64), dimension(:,:), allocatable :: x, fx
    integer :: i, repeat

    allocate (x(N,N), fx(N,N))
    repeat = 50

    x = 1.0d0

    do i = 1, repeat
        ! call sub2 (x, fx)
        fx = func2 (x)
    end do

    print *, x(1,1)

contains

impure elemental subroutine sub1 (x, fx)
    real (real64), intent(in) :: x
    real (real64), intent(out) :: fx

    fx = x * 1.01d0
end subroutine

impure elemental function func1 (x) result(fx)
    real (real64), intent(in) :: x
    real (real64) :: fx

    fx = x * 1.01d0
end function

pure subroutine sub2 (x, fx)
    real (real64), intent(in), dimension(:,:) :: x
    real (real64), intent(out), dimension(:,:) :: fx

    fx = x * 1.01d0
end subroutine

pure function func2 (x) result(fx)
    real (real64), intent(in), dimension(:,:) :: x
    real (real64), dimension(N,N) :: fx

    ! real (real64), dimension(N,N) :: foo
    !
    ! foo = 0

    fx = x * 1.01d0
    ! fx(1,1) = foo(1,1)
end function

end program
