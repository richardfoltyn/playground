module mod2
    use iso_fortran_env
contains
subroutine compute_impl (x, fx, param1, param2)
    real (real64), intent(in) :: x
    real (real64), intent(out) :: fx
    real (real64), intent(in) :: param1, param2

    fx = param1 + param2 * x
end subroutine
end module

module mod1

    use iso_fortran_env
    use mod2
    implicit none

    type :: foo_t
        real (real64) :: param1, param2
    contains
        procedure, pass :: compute_stuff
    end type

contains

impure elemental subroutine compute_stuff (self, x, fx)
    class (foo_t), intent(in) :: self
    real (real64), intent(in) :: x
    real (real64), intent(out) :: fx

    call compute_impl (x, fx, self%param1, self%param2)
end subroutine
end module

program test_elemental

    use mod1

    type (foo_t) :: foo = foo_t(1.0d0, 2.0d0)
    real (real64), dimension(10) :: x, fx
    real (real64) :: x1, fx1
    integer :: i

    forall (i = 1:size(x)) x(i) = i * 2.0d0

    call foo%compute_stuff (x, fx)

    print *, x
    print *, fx

    x1 = 10.0d0
    call foo%compute_stuff (x1, fx1)
    print *, x1, fx1

end program
