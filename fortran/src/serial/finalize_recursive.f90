module type1_mod
 
    implicit none

    type :: type1
        character (10) :: name
        class (*), pointer :: item => null()
    contains
        final :: type1_finalizer
    end type

    type :: type2
        character (10) :: name
        class (*), allocatable :: item
    contains
        final :: type2_finalizer
    end type

contains

subroutine type1_finalizer (self)
    type (type1), intent(in out) :: self
    print *, "type1 finalizer called: " // trim(self%name)
    if (associated(self%item)) deallocate(self%item)
end subroutine

subroutine type2_finalizer (self)
    type (type2), intent(in out) :: self
    print *, "type2 finalizer called: " // trim(self%name)
    if (allocated(self%item)) deallocate(self%item)
end subroutine

end module

program finalize_recursive

    use type1_mod
    implicit none

    call main ()
contains 

subroutine main ()
    type (type1) :: obj1, obj3
    type (type2) :: obj2
    
    obj1%name = "obj1"
    obj2%name = "obj2"
    obj3%name = "obj3"

    allocate (obj2%item, source=obj3)
    allocate (obj1%item, source=obj2)
end subroutine

end program
